Git pronunciado [git] (ou pronunciado [dit] em inglês britânico) é um sistema de
controle de versão distribuído e um sistema de gerenciamento de código fonte, com
ênfase em velocidade. O Git foi inicialmente projetado e desenvolvido por Linus
Torvalds para o desenvolvimento do kernel Linux, mas foi adotado por muitos outros
projetos.

Cada diretório de trabalho do Git é um repositório com um histórico completo
e habilidade total de acompanhamento das revisões, não dependente de acesso
a uma rede ou a um servidor central.

O Git é um software livre, distribuído sob os termos da versão 2 da GNU
General Public License. Sua manutenção é atualmente supervisionada por
Junio Hamano.

Texto retirado de
https://pt.wikipedia.org/wiki/Git

==========
João Daniel - 2017
==========
